import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

const PATH = "/crm/opportunity/entities/opportunity";
let response;

const crmListOpportunity = async (event) => {
  const url = event.headers['x-platform-environment'] + PATH;

  try {
    const responseData = await Axios.get(url, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });
    
    response = {
      statusCode: responseData?.status || 200,
      body: JSON.stringify({
        contents: responseData.data.contents.map(content => {
          return {
            opportunity: content.opportunityName,
            origin: content.origin.name,
            negotiationStage: content.negotiationStage.name,
            probability: `${content.probability}%`
          }
        })
      })
    };
  } catch (error) {
    console.log(error);
    response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
  return response;
};

export const main = middyfy(crmListOpportunity);