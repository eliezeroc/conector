/* eslint-disable  @typescript-eslint/no-explicit-any */
import Axios from 'axios';
import { middyfy } from '../../libs/lambda';

const PATH = '/platform/notifications/actions/notifyUser';

const notifyUser = async (event) => {
  try {

    let to = "";
    if (event.body?.destinationUsers) {
      try {
        to = JSON.parse(event.body?.destinationUsers);
        to = JSON.stringify(to);
      } catch (err) {
        if (event.body?.destinationUsers.split(',').length) {
          to = event.body?.destinationUsers.trim().split(',');
        } else {
          to = [event.body?.destinationUsers] as any;
        }
        to = JSON.stringify(to);
      }
    }

    const url = event.headers['x-platform-environment'] + PATH;

    const body = event.body;

    if (!event.body?.sourceDomain && !event.body?.sourceService) {
      body.sourceDomain = 'platform';
      body.sourceService = 'conector';
    }

    const emails = JSON.parse(to);
    body.destinationUsers = emails.map(e => e.split('@')[0]);

    body.notificationOrigin = 'conector';

    body.notificationPriority = body.notificationPriority === 'Alerta' ? 'Alert' : (body.notificationPriority === 'Erro' ? 'Error' : 'None');

    body.notificationKind = body.notificationKind === 'Gerencial' ? 'Management' : (body.notificationKind === 'Novidades' ? 'News' : 'Operational');

    const responseData = await Axios.post(url, body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseData?.data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(notifyUser);
