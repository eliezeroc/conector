import { middyfy } from '../../libs/lambda';
import { Client, TextContent } from '@zenvia/sdk';
import { validatePhoneNumber } from '../../libs/utils';
const sendWhatsapp = async (event) => {
  try {
    const { destination, content, token, sender } = event.body;

    const phoneNumber = validatePhoneNumber(destination);

    const client = new Client(token);

    const whatsapp = client.getChannel('whatsapp');

    const message = new TextContent(content);

    const result = await whatsapp.sendMessage(sender, phoneNumber, message);

    return {
      statusCode: 200,
      body: JSON.stringify(result)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendWhatsapp);
