/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';
import moment from 'moment';
import Axios from 'axios';

const PATH_GET_USER = '/platform/user/queries/getUser'

const PATH_ACTIVE_EMPLOYEE = '/hcm/analytics/queries/activeEmployeeFilter'

const PATH_VACATION_PERIOD = '/hcm/vacationmanagement/entities/vacationperiod';

const PATH_LEADER_USER = '/hcm/analytics/queries/getEmployeeFirstActiveLeaderUser';

let url;
let responseData;

const vacationPeriod = async (event) => {
  try {    
    url = event.headers['x-platform-environment'] + PATH_GET_USER;

    responseData = await Axios.get(url, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    url = event.headers['x-platform-environment'] + PATH_ACTIVE_EMPLOYEE;    
    
    responseData = await Axios.get(url, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    const dataActiveEmployee = responseData.data; 

    if (dataActiveEmployee && !dataActiveEmployee.value) {
      return {
        statusCode: 404,
        body: 'Não foi possível encontrar o colaborador vinculado ao usuário logado'
      };
    }

    url = event.headers['x-platform-environment'] + PATH_VACATION_PERIOD + `?filter=employee='${dataActiveEmployee.value}'` + ` and situation = 'OPENED'`;
    responseData = await Axios.get(url, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    url = event.headers['x-platform-environment'] + PATH_LEADER_USER
    const responseDataLeader = await Axios.post(url, { employeeId: dataActiveEmployee.value}, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    const data = responseData.data;
    const responseList = [] as any[];

    data.contents.forEach(vacationPeriod => {
      const vacationPeriodFormated = {
        employeeId: vacationPeriod.employee,
        leaderUser: responseDataLeader.data.leaderUser,
        periodId: vacationPeriod.id,
        startDate: moment(vacationPeriod.startdate).format("DD/MM/YYYY"),
        endDate: moment(vacationPeriod.enddate).format("DD/MM/YYYY"),
        limitDate: vacationPeriod.limitdate,
        leaveBalance: vacationPeriod.leavebalance,
        periodWithBalance: `${moment(vacationPeriod.startdate).format("DD/MM/YYYY")} - ${moment(vacationPeriod.enddate).format("DD/MM/YYYY")} - Saldo atual: ${vacationPeriod.leavebalance}`
      };
    
      responseList.push(vacationPeriodFormated);
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(responseList)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(vacationPeriod);