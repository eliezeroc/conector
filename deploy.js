/* eslint-disable no-undef */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const exec = require('child_process').exec;


// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');

const deploy = async () => {
  const changes = new Promise((resolve, reject) => {
    console.log(`git diff --name-only ${process.env.CI_COMMIT_BEFORE_SHA} ${process.env.CI_COMMIT_SHA}`);
    exec(`git diff --name-only ${process.env.CI_COMMIT_BEFORE_SHA} ${process.env.CI_COMMIT_SHA}`, (err, stdout) => {
      if (err) {
        reject(err);
      }
      resolve(stdout);
    })
  });
  const itens = await changes;
  const avalilableChanges = [];
  itens.split('\n').filter(item => item.includes('conectores/') || item.includes('assignments/')).forEach(item => {
    const available = item.split('/');
    if (!avalilableChanges.includes(available[1])) {
      avalilableChanges.push(available[1]);
    }
  });

  console.log('Deploy of: ', avalilableChanges);

  for (let avalilableChange of avalilableChanges) {

    const servelerssDeplyPromise = new Promise((resolve, reject) => {
      exec(`serverless deploy function --function ${avalilableChange}`, (err, stdout) => {
        if (err) {
          reject(err);
        }
        resolve(stdout);
      });
    });
    console.log(await servelerssDeplyPromise);
  }
};

deploy();