import { middyfy } from '../../libs/lambda';
import oracledb from 'oracledb';

const handler = async (event) => {

  if (!event?.body?.connectString  || !event?.body?.username || !event?.body?.password || !event?.body?.externalAuth  ) {
    return {
      statusCode: 400,
      body: 'Para usar este conector informe o connectString, username, password e externalAuth para se conectar no oracle'
    };
  }

  const { connectString, username, password, externalAuth } = event.body;

  const connection = await oracledb.getConnection({
    user: username,
    connectString,
    password,
    externalAuth
  });

  const result = await connection.execute('select * from pessoa');

  await connection.close();

  return {
    statusCode: 200,
    body: JSON.stringify(result.rows)
  };
};

export const main = middyfy(handler);