export const validatePhoneNumber = (phoneNumber: string) => {
    phoneNumber = phoneNumber.replace('(', '').replace(')', '').replace('-', '').replace('+', '').replace(/\s/g,'');
    if (phoneNumber.length === 11) {
        phoneNumber = `55${phoneNumber}`;
    }
    return phoneNumber;
}